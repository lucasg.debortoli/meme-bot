class MemeBot
    add_handler Handler::CallbackQuery.new(:callback_confirm_ban, 'confirm_ban')

    def callback_confirm_ban(callback)
        return unless authorized(callback, callback: true, action: 'apretar ese botón',
                                           lower_perm: true)

        match = callback.data.match(/\Aconfirm_ban:(?<temp_id>\d+):(?<action>.+)\z/)

        return unless can_press_keyboard?(
            callback.id,
            callback.message.chat.id,
            callback.message.message_id,
            'confirm_ban',
            'Este tablero ya fue utilizado, si siguen apareciendo los botones es '\
            'porque hubo un error de telegram al editar ese mensaje.'
        )

        temp_id = match['temp_id']
        return if self_callback_request(callback, temp_id)

        case match['action']
        when 'yes'
            ban_user temp_id, callback
        when 'no'
            keywords = @redis.hget "memes:temp:#{temp_id}", 'keywords'
            back_to_callback_confirm_reject temp_id, keywords, callback
        end
    end

    private

    def send_confirm_ban(temp_id, callback)
        options = Telegram::Bot::Types::InlineKeyboardMarkup.new inline_keyboard: [[
            Telegram::Bot::Types::InlineKeyboardButton.new(
                text: 'Sí',
                callback_data: "confirm_ban:#{temp_id}:yes"
            ),
            Telegram::Bot::Types::InlineKeyboardButton.new(
                text: 'No',
                callback_data: "confirm_ban:#{temp_id}:no"
            )
        ]]

        chat_id = callback.message.chat.id
        message_id = callback.message.message_id

        @tg.edit_message_text(
            chat_id: chat_id,
            message_id: message_id,
            text: '¿Borrar el video y bloquear a quien lo envió?',
            reply_markup: options,
            disable_web_page_preview: true,
            disable_notification: true
        )

        @redis.set("keyboard:#{chat_id}:#{message_id}", 'confirm_ban')
    end

    def ban_user(temp_id, callback)
        chat_id = callback.message.chat.id
        message = callback.message
        message_id = message.message_id

        meme_data = delete_temp_keys temp_id, chat_id, message_id
        origin_chat_id = meme_data['chat_id']
        origin_user_id = meme_data['user_id']

        @redis.srem("memes:temp_group:#{origin_chat_id}", temp_id)
        @redis.sadd('user_blacklist', origin_user_id)

        # Puede fallar en cualquiera de estas 3 llamadas a la api asi que mejor
        # borrar las claves y bloquear al usuario antes por las dudas
        @tg.delete_message(
            chat_id: chat_id,
            message_id: message.reply_to_message.message_id
        )

        @tg.edit_message_text(
            chat_id: chat_id,
            message_id: message_id,
            parse_mode: :html,
            disable_web_page_preview: true,
            disable_notification: true,
            text: "Meme <code>#{meme_data['keywords']}</code> rechazado "\
                  "y usuario #{origin_user_id} BLOQUEADO por "\
                  "#{user_link callback.from, message.chat} "\
                  "(#{callback.from.id})"
        )

        @tg.send_message(
            chat_id: origin_chat_id,
            reply_to_message_id: meme_data['message_id'],
            text: 'No podrás usar más el bot por haber pasado eso'
        )
    end
end
