class MemeBot
    add_handler Handler::Command.new(
        :memeid,
        :give_meme_by_id,
        allow_params: true,
        description: 'Envía un meme dado su ID'
    )

    def give_meme_by_id(msg, params)
        return unless authorized(msg, action: 'usar este comando')

        unless params
            @tg.send_message(
                chat_id: msg.chat.id,
                reply_to_message_id: msg.message_id,
                text: 'Necesito que me pases una ID para buscar'
            )
            return
        end

        meme_data = @redis.hmget("meme:#{params}", %i[video_id keywords])

        unless meme_data.any?
            @tg.send_message(
                chat_id: msg.chat.id,
                reply_to_message_id: msg.message_id,
                text: 'No hay ningún meme con esa ID'
            )
            return
        end

        @tg.send_video(
            chat_id: msg.chat.id,
            reply_to_message_id: msg.message_id,
            video: meme_data.first,
            caption: "<b>#{meme_data.last}</b>",
            parse_mode: :html
        )
    end
end
