class MemeBot
    add_handler Handler::Command.new(
        :salir,
        :exit,
        allowed_chats: %w[group supergroup]
    )

    def exit(msg)
        return unless authorized(msg, action: 'usar este comando')

        unless msg.reply_to_message && msg.reply_to_message.from.id == @user.id
            @tg.send_message(
                chat_id: msg.chat.id,
                reply_to_message_id: msg.message_id,
                text: 'Tienes que responder un mensaje mío con ese comando '\
                      'para que me vaya de este chat'
            )
            return
        end

        @tg.send_message(
            chat_id: msg.chat.id,
            reply_to_message_id: msg.message_id,
            text: 'Adiooos 👋👋🏻👋🏼👋🏽👋🏾👋🏿'
        )

        @tg.leave_chat(chat_id: msg.chat.id)
    end
end
