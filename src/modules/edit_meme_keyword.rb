class MemeBot
    add_handler Handler::Command.new(
        :editarmeme,
        :editmeme,
        allow_params: true,
        description: 'Cambia el nombre de un meme'
    )

    def editmeme(msg, params)
        return unless authorized(msg, action: 'usar este comando')
        return unless (values = validate_editmeme_params msg, params)

        id = values[:id]
        final_text = values[:final_text]
        old_keyword = values[:old_keyword]

        # Actualizar db
        @redis.srem "keywords_match:#{old_keyword}", id
        if @redis.scard("keywords_match:#{old_keyword}").zero?
            @redis.del "keywords_match:#{old_keyword}"
            @redis.srem 'memes', old_keyword
        end

        @redis.hset "meme:#{id}", 'keywords', final_text
        @redis.sadd "keywords_match:#{final_text}", id
        @redis.sadd 'memes', final_text

        @tg.send_message(
            chat_id: msg.chat.id,
            parse_mode: :html,
            text: "Meme <code>#{id}</code> actualizó su nombre de "\
                  "#{old_keyword} a #{final_text}"
        )
    end

    private

    def validate_editmeme_params(msg, params)
        if params.nil? || (words = params.split).length <= 1
            @tg.send_message(
                chat_id: msg.chat.id,
                reply_to_message_id: msg.message_id,
                parse_mode: :html,
                text: "Tienes que usar el comando de la siguiente forma: \n"\
                      '/editmeme <code>id_meme frase</code>'
            )
            return false
        end

        id = words.first
        old_keyword = @redis.hget "meme:#{id}", 'keywords'

        if old_keyword.nil?
            @tg.send_message(
                chat_id: msg.chat.id,
                reply_to_message_id: msg.message_id,
                text: 'El ID que me pasaste no corresponde al de ningún '\
                      'meme guardado actualmente'
            )
            return false
        end

        keywords = words[1..].join ' '
        final_text = parse_keywords(keywords)

        return false if no_keywords(msg, final_text, true)
        return false if wrong_size_name(msg, final_text.length, true)

        if final_text == old_keyword
            @tg.send_message(
                chat_id: msg.chat.id,
                reply_to_message_id: msg.message_id,
                parse_mode: :html,
                text: 'El nuevo nombre para el meme que me enviaste resultó en el '\
                      "mismo con el que ya estaba guardado: <code>#{old_keyword}</code>"
            )
            return false
        end

        { id: id, final_text: final_text, old_keyword: old_keyword }
    end
end
