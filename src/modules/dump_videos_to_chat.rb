class MemeBot
    add_handler Handler::Command.new(
        :dump_videos,
        :send_menu_dump_videos
    )

    add_handler Handler::CallbackQuery.new(:callback_dump_videos, 'dump_videos')

    def send_menu_dump_videos(msg)
        return unless authorized(msg, action: 'usar este comando')

        arr = { 'Confirmar' => 'confirm', 'Cancelar' => 'cancel' }.map do |text, val|
            Telegram::Bot::Types::InlineKeyboardButton.new(
                text: text, callback_data: "dump_videos:#{val}"
            )
        end

        options = Telegram::Bot::Types::InlineKeyboardMarkup.new(inline_keyboard: [arr])

        @tg.send_message(
            chat_id: msg.chat.id,
            reply_markup: options,
            text: '¿Desea enviar todos los videos guardados al canal de videos?'
        )
    end

    def callback_dump_videos(callback)
        return unless authorized(callback, callback: true, action: 'apretar ese botón')
        return unless (match = callback.data.match(/dump_videos:(?<action>\w+)/))

        chat_id = callback.message.chat.id
        message_id = callback.message.message_id

        case match['action']
        when 'cancel'
            @tg.answer_callback_query(
                callback_query_id: callback.id,
                text: 'Cancelado'
            )

        when 'confirm'
            dump_videos chat_id, @channel_videos
        end

        @tg.delete_message(
            chat_id: chat_id,
            message_id: message_id
        )
    end

    private

    def dump_videos(chat_id_origin, chat_send_videos)
        @redis.smembers('memes').each do |keywords|
            @redis.smembers("keywords_match:#{keywords}").each do |meme_id|
                meme_data = @redis.hgetall("meme:#{meme_id}")

                begin
                    @tg.send_video(
                        chat_id: chat_send_videos,
                        video: meme_data['video_id'],
                        caption: "<i>#{meme_data['keywords']}</i>",
                        parse_mode: :html
                    )
                    sleep 3
                rescue Telegram::Bot::Exceptions::ResponseError => e
                    @logger.error(
                        "Video #{meme_data['video_id']}, "\
                        "keyword #{meme_data['keywords']} #{e}",
                        to_channel: true
                    )
                end
            end
        end

        @tg.send_message(
            chat_id: chat_id_origin,
            text: 'Videos enviados'
        )
    end
end
