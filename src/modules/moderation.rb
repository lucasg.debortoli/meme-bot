class MemeBot
    add_handler Handler::Command.new(
        :bloquear,
        :block,
        allow_params: true,
        description: 'Bloquea a un usuario para que no pueda usar el bot'
    )

    add_handler Handler::Command.new(
        :desbloquear,
        :unblock,
        allow_params: true,
        description: 'Desbloquea a un usuario para que pueda volver a usar el bot'
    )

    add_handler Handler::Command.new(
        :usuariosbloqueados,
        :list_blocked_users,
        description: 'Lista de usuarios bloqueados'
    )

    add_handler Handler::Command.new(
        :bloquearchat,
        :chat_block,
        allow_params: true,
        description: 'Bloquea a un chat para que desde allí no se pueda usar el bot'
    )

    add_handler Handler::Command.new(
        :desbloquearchat,
        :chat_unblock,
        allow_params: true,
        description: 'Desbloquea un chat para que se pueda volver a usar el bot allí'
    )

    add_handler Handler::Command.new(
        :chatsbloqueados,
        :list_blocked_chats,
        description: 'Lista de chats bloqueados'
    )

    def block(msg, params)
        return unless authorized(msg, action: 'usar este comando',
                                      lower_perm: true)

        user_id = give_user_id_affected(msg, params)&.to_i
        return unless user_id

        if AUTORITIES.include?(user_id) ||
           (LOWER_PERMISSIONS.include?(user_id) && !AUTORITIES.include?(msg.from.id))
            @tg.send_message(
                chat_id: msg.chat.id,
                reply_to_message_id: msg.message_id,
                text: 'No se puede bloquear a ese usuario'
            )
            return
        end

        if @redis.sismember('user_blacklist', user_id)
            @tg.send_message(
                chat_id: msg.chat.id,
                reply_to_message_id: msg.message_id,
                text: 'Ese usuario ya fue bloqueado'
            )
            return
        end

        @redis.sadd('user_blacklist', user_id)

        @tg.send_message(
            chat_id: msg.chat.id,
            reply_to_message_id: msg.message_id,
            text: "Usuario con id #{user_id} bloqueado"
        )
    end

    def unblock(msg, params)
        return unless authorized(msg, action: 'usar este comando',
                                      lower_perm: true)

        user_id = give_user_id_affected(msg, params)
        return unless user_id

        unless @redis.sismember('user_blacklist', user_id)
            @tg.send_message(
                chat_id: msg.chat.id,
                reply_to_message_id: msg.message_id,
                text: 'Ese usuario no está bloqueado'
            )
            return
        end

        @redis.srem('user_blacklist', user_id)

        @tg.send_message(
            chat_id: msg.chat.id,
            reply_to_message_id: msg.message_id,
            text: "Usuario con id #{user_id} desbloqueado"
        )
    end

    def chat_block(msg, params)
        return unless authorized(msg, action: 'usar este comando')

        unless params && params.length <= 25 && /-\d+/.match?(params)
            @tg.send_message(
                chat_id: msg.chat.id,
                reply_to_message_id: msg.message_id,
                text: 'Tienes que pasarme un id de chat válida'
            )
            return
        end

        if [@channel_logging, @channel_memes].include? params.to_i
            @tg.send_message(
                chat_id: msg.chat.id,
                reply_to_message_id: msg.message_id,
                text: 'No puedes bloquear esa ID'
            )
            return
        end

        if @redis.sismember('chat_blacklist', params)
            @tg.send_message(
                chat_id: msg.chat.id,
                reply_to_message_id: msg.message_id,
                text: 'Ese chat ya fue bloqueado'
            )
            return
        end

        @redis.sadd('chat_blacklist', params)

        @tg.send_message(
            chat_id: msg.chat.id,
            reply_to_message_id: msg.message_id,
            text: "Chat con ID #{params} bloqueado"
        )
    end

    def chat_unblock(msg, params)
        return unless authorized(msg, action: 'usar este comando')

        unless params && params.length <= 25 && /-\d+/.match?(params)
            @tg.send_message(
                chat_id: msg.chat.id,
                reply_to_message_id: msg.message_id,
                text: 'Tienes que pasarme un id de chat válida'
            )
            return
        end

        unless @redis.sismember('chat_blacklist', params)
            @tg.send_message(
                chat_id: msg.chat.id,
                reply_to_message_id: msg.message_id,
                text: 'Ese chat no está bloqueado'
            )
            return
        end

        @redis.srem('chat_blacklist', params)

        @tg.send_message(
            chat_id: msg.chat.id,
            reply_to_message_id: msg.message_id,
            text: "Chat con ID #{params} desbloqueado"
        )
    end

    def list_blocked_users(msg)
        return unless authorized(msg, action: 'usar este comando')

        send_blacklist(
            msg,
            'No hay usuarios bloqueados',
            'user_blacklist',
            "Usuarios bloqueados\n"
        )
    end

    def list_blocked_chats(msg)
        return unless authorized(msg, action: 'usar este comando')

        send_blacklist(
            msg,
            'No hay chats bloqueados',
            'chat_blacklist',
            "Chats bloqueados\n"
        )
    end

    private

    def send_blacklist(msg, empty_error, key, title)
        if (set_members = @redis.smembers(key)).empty?
            @tg.send_message(
                chat_id: msg.chat.id,
                reply_to_message_id: msg.message_id,
                text: empty_error
            )
            return
        end

        arr = []

        add_line = proc { |item| "=> <code>#{item}</code>\n" }

        board_array(
            arr: arr,
            title: title,
            counter: 0,
            max_lines: 30,
            max_size: 1000,
            code_block: add_line,
            iterable_set: set_members.sort
        )

        options = armar_botonera(0, arr.size, msg.from.id)
        chat_id = msg.chat.id
        resp = @tg.send_message(chat_id: chat_id,
                                parse_mode: :html,
                                reply_markup: options,
                                text: arr.first,
                                disable_notification: true)

        return unless resp

        armar_lista(
            chat_id,
            Telegram::Bot::Types::Message.new(resp['result']).message_id,
            arr
        )
    end

    def give_user_id_affected(msg, params)
        if params
            return params if params.length <= 25 && /\d+/.match?(params)

            @tg.send_message(
                chat_id: msg.chat.id,
                reply_to_message_id: msg.message_id,
                text: 'ID inválida'
            )
            nil
        elsif msg.reply_to_message
            msg.reply_to_message.from.id
        else
            @tg.send_message(
                chat_id: msg.chat.id,
                reply_to_message_id: msg.message_id,
                text: 'Tienes que responder a un mensaje o pasarme una ID'
            )
            nil
        end
    end
end
