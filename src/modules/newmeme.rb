require 'securerandom'

class MemeBot
    add_handler Handler::Command.new(
        :nuevomeme,
        :new_meme,
        allow_params: true,
        description: 'Envía petición para agregar un meme al bot'
    )

    def new_meme(msg, params)
        return unless correct_params(msg, params)

        add_meme(msg, msg.reply_to_message.video, params,
                 by_user: true)
    end

    private

    def correct_params(msg, params)
        unless params && msg.reply_to_message&.video
            @tg.send_message(
                chat_id: msg.chat.id,
                parse_mode: :html,
                reply_to_message_id: msg.message_id,
                text: "<strong>Requisitos para un nuevo video meme:</strong>\n\n"\
                      "- Menos de 10 segundos\n"\
                      "- Un peso menor de 1,5MB\n"\
                      "- Y que sea un video ideal para usar en una conversación\n\n"\
                      "Puede consultar los videos ya ingresados en @BibliotecaMeme\n\n"\
                      "<strong>Modo de uso:</strong>\n\n"\
                      'Para agregar un video al bot tenés que responder un '\
                      "video con el siguiente texto\n\n"\
                      '"<code>/nuevomeme frase a guardar</code>"'
            )
            return false
        end

        if msg.reply_to_message.video.duration > 10
            @tg.send_message(
                chat_id: msg.chat.id,
                reply_to_message_id: msg.message_id,
                text: 'No se permiten videos de más de 10 segundos'
            )
            return false
        end

        true
    end

    def add_meme(msg, video, params, by_user: false)
        video_id = video.file_id
        return if petitioned_video(msg, video_id, by_user)

        final_text = parse_keywords(params)
        return if no_keywords(msg, final_text, by_user)
        return if wrong_size_name(msg, final_text.length, by_user)
        return if invalid_mime_type(msg, video.mime_type)

        unless by_user
            add_meme_in_database(
                final_text,
                video_id,
                msg.message_id,
                @owner,
                msg.chat.id
            )
            return
        end

        if AUTORITIES.include?(msg.from.id)
            add_meme_in_database(
                final_text,
                video_id,
                msg.message_id,
                msg.from,
                msg.chat.id
            )
        else
            send_petition_new_meme(
                msg,
                final_text,
                video_id
            )
        end
    end

    def add_meme_in_database(keywords, video_id, msg_id, user, chat_id)
        # En este caso es cuando se le pasa una id en user, si no se pasa un objeto user
        if user == @owner
            user_id = user
            added = 'a través del chat de videos.'
        else
            user_id = user.id
            added = "por #{user_link user, chat_id} (#{user_id})"
        end

        meme_id = modify_database_newmeme keywords, video_id, msg_id, user_id, chat_id

        @tg.send_message(
            chat_id: chat_id,
            reply_to_message_id: msg_id,
            parse_mode: :html,
            text: "Meme <i>#{keywords}</i> (<code>#{meme_id}</code>) agregado #{added}",
            disable_web_page_preview: true,
            disable_notification: true
        )

        @tg.send_video(
            chat_id: @channel_videos,
            parse_mode: :html,
            video: video_id,
            caption: "<i>#{keywords}</i>"
        )
    end

    def modify_database_newmeme(keywords, video_id, _msg_id, user_id, chat_id)
        @redis.sadd('memes', keywords)

        meme_id = generate_meme_id
        contenido = {
            user_id: user_id || @owner,
            chat_id: chat_id || @channel_memes,
            keywords: keywords,
            video_id: video_id
        }
        @redis.mapped_hmset("meme:#{meme_id}", contenido)
        @redis.sadd("keywords_match:#{keywords}", meme_id)
        meme_id
    end

    def no_keywords(msg, keywords, by_user)
        if keywords.empty?
            if by_user
                @tg.send_message(
                    chat_id: msg.chat.id,
                    reply_to_message_id: msg.message_id,
                    text: 'El nombre del meme no es un texto válido'
                )
            end
            return true
        end
        false
    end

    def wrong_size_name(msg, total_size, by_user)
        if by_user
            if total_size < 5
                @tg.send_message(
                    chat_id: msg.chat.id,
                    reply_to_message_id: msg.message_id,
                    text: 'Nombre de meme muy corto, (o con pocos caracteres válidos) '\
                          'puedes probar con uno más largo'
                )
                return true
            elsif total_size > 65
                @tg.send_message(
                    chat_id: msg.chat.id,
                    reply_to_message_id: msg.message_id,
                    text: 'Nombre demasiado largo, puedes probar con uno más corto'
                )
                return true
            end
        end
        false
    end

    def invalid_mime_type(msg, mime_type)
        if mime_type != 'video/mp4'
            @tg.send_message(
                chat_id: msg.chat.id,
                reply_to_message_id: msg.message_id,
                text: 'El formato del archivo es inválido, pruebe convirtiéndolo en mp4'
            )
            return true
        end
        false
    end

    def generate_meme_id
        loop do
            id = SecureRandom.hex(4)
            return id unless @redis.exists?("meme:#{id}")
        end
    end

    def petitioned_video(msg, video_id, by_user)
        if by_user && (member = @redis.sismember('memes:temp_videos', video_id))
            @tg.send_message(
                chat_id: msg.chat.id,
                reply_to_message_id: msg.message_id,
                text: 'Este video ya está esperando ser aprobado o no'
            )
        end
        member
    end
end
