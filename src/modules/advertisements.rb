class MemeBot
    add_handler Handler::Command.new(
        :anunciar,
        :announce,
        allow_params: true,
        description: 'Envía un mensaje a todos los chats donde se encuentra el bot '\
                     '(usar con cuidado)'
    )

    add_handler Handler::Message.new(
        :regist_chats,
        allowed_chats: %w[private group supergroup channel]
    )

    add_handler Handler::ChatEvent.new(
        :regist_chats_supergroup,
        types: [:migrate_from_chat_id],
        allowed_chats: %i[supergroup]
    )

    def announce(msg, params)
        return unless authorized(msg, action: 'usar este comando')

        if params.nil? || params.empty?
            @tg.send_message(
                chat_id: msg.chat.id,
                text: 'Necesito que me pases el mensaje a enviar a todos los chats'
            )
            return
        end

        send_announce(params, 'private')
        send_announce(params, 'group')
        send_announce(params, 'supergroup')
    end

    def regist_chats(msg)
        @redis.sadd "chats:#{msg.chat.type}", msg.chat.id
    end

    def regist_chats_supergroup(msg)
        @redis.srem 'chats:group', msg.migrate_from_chat_id
    end

    private

    def send_announce(text, chat_type)
        @redis.smembers("chats:#{chat_type}").each do |chat_id|
            @tg.send_message(chat_id: chat_id.to_i, text: text)
        rescue Telegram::Bot::Exceptions::ResponseError => e
            case (error = e.message)
            when /
                (bot\ is\ not\ a\ member\ of\ the\ (super)?group\ chat)|
                (bot\ was\ kicked\ from\ the\ (super)?group\ chat)|
                (bot\ was\ blocked\ by\ the\ user)|
                (chat\ not\ found)|
                (PEER_ID_INVALID)|
                (bot is not a member of the channel chat)
            /x
                redis.srem "chats:#{chat_type}", chat_id
            else
                @logger.error 'Error enviando un mensaje al '\
                              "chat #{chat_id}: #{error}"
            end
        end
    end
end
