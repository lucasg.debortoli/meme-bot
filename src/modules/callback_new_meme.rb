class MemeBot
    add_handler Handler::CallbackQuery.new(:callback_new_meme, 'new_meme')

    add_handler Handler::ChatEvent.new(
        :temp_keys_supergroup,
        types: [:migrate_from_chat_id],
        allowed_chats: %i[supergroup]
    )

    def callback_new_meme(callback)
        return unless authorized(callback, callback: true, action: 'apretar ese botón',
                                           lower_perm: true)

        match = callback.data.match(/\Anew_meme:(?<temp_id>\d+):(?<action>.+)\z/)

        return unless can_press_keyboard?(
            callback.id,
            callback.message.chat.id,
            callback.message.message_id,
            'to_press',
            'Este tablero ya fue utilizado, si siguen apareciendo los botones es '\
            'porque hubo un error de telegram al editar ese mensaje.'
        )

        temp_id = match['temp_id']
        return if self_callback_request(callback, temp_id)

        case match['action']
        when 'confirm'
            execute_confirm_callback callback, temp_id
        when 'reject'
            send_reject_options temp_id, callback
        when 'ban'
            send_confirm_ban temp_id, callback
        end
    rescue Telegram::Bot::Exceptions::ResponseError => e
        @logger.warn e.to_s, to_channel: true
    end

    def temp_keys_supergroup(msg)
        id_dest = msg.chat.id
        id_orig = msg.migrate_from_chat_id

        # Temp keys
        temp_id_set = "memes:temp_group:#{id_orig}"

        @redis.smembers(temp_id_set).each do |temp_id|
            @redis.hset("memes:temp:#{temp_id}", 'chat_id', id_dest)
        end

        @redis.del(temp_id_set)

        # Meme keys
        meme_group_set = "memes:group:#{id_orig}"

        @redis.smembers(meme_group_set).each do |meme_id|
            @redis.hset("meme:#{meme_id}", 'chat_id', id_dest)
        end

        @redis.del(meme_group_set)
    end

    private

    #### Send keyboard ####
    def send_petition_new_meme(msg, keywords, video_id)
        count = add_temp_key(msg, keywords, video_id)
        options = create_petition_keyboard count

        text = create_msg_keyboard_temp_meme(msg, keywords, video_id, 'agregar')
        msg_log = @tg.send_message(
            chat_id: @channel_logging,
            parse_mode: :html,
            text: text,
            disable_web_page_preview: true,
            disable_notification: true
        )

        unless msg_log['result'] && msg_log['result']['message_id']
            @logger.error "Problema con el mensaje: #{msg_log}", to_channel: true
            return
        end
        id_msg_log = msg_log['result']['message_id'].to_i

        # Mando el video al canal
        video_sent = @tg.send_video(
            chat_id: @channel_logging,
            video: video_id,
            reply_to_message_id: id_msg_log
        )
        id_video_msg = video_sent['result']['message_id'].to_i

        # Mando el tablero para aceptar o rechazarlo
        message_buttons = @tg.send_message(
            chat_id: @channel_logging,
            parse_mode: :html,
            text: "¿Aceptar el meme <code>#{keywords}</code>?",
            reply_markup: options,
            disable_web_page_preview: true,
            disable_notification: true,
            reply_to_message_id: id_video_msg
        )

        message_id = message_buttons['result']['message_id']
        @redis.set "keyboard:#{@channel_logging}:#{message_id}", 'to_press'

        @tg.send_message(
            chat_id: msg.chat.id,
            reply_to_message_id: msg.message_id,
            text: 'Su propuesta de video ha sido enviada con éxito, ahora pasara por '\
                  'una evaluación por parte de los administradores para '\
                  "ser aceptado.\n\n"\
                  'Pude verificar su aceptación en el canal @BibliotecaMeme'
        )
    end

    def create_petition_keyboard(temp_id)
        Telegram::Bot::Types::InlineKeyboardMarkup.new inline_keyboard: [
            [
                Telegram::Bot::Types::InlineKeyboardButton.new(
                    text: 'Aceptar',
                    callback_data: "new_meme:#{temp_id}:confirm"
                ),
                Telegram::Bot::Types::InlineKeyboardButton.new(
                    text: 'Rechazar',
                    callback_data: "new_meme:#{temp_id}:reject"
                )
            ],
            [
                Telegram::Bot::Types::InlineKeyboardButton.new(
                    text: 'Bloquear y borrar',
                    callback_data: "new_meme:#{temp_id}:ban"
                )
            ]
        ]
    end

    def add_temp_key(msg, keywords, video_id)
        temp_data = {
            message_id: msg.message_id,
            chat_id: msg.chat.id,
            video_id: video_id,
            user_id: msg.from.id,
            keywords: keywords
        }

        count = @redis.incr('memes:temp')

        @redis.mapped_hmset("memes:temp:#{count}", temp_data)

        @redis.sadd('memes:temp_videos', video_id)
        @redis.sadd("memes:temp_group:#{msg.chat.id}", count) if msg.chat.type == 'group'

        count
    end

    #### Aux callback ####

    def execute_confirm_callback(callback, temp_id)
        meme_data = confirm_new_meme_callback temp_id, callback

        executer = user_link(callback.from, callback.message.chat.id)
        meme_id = (id = meme_data['meme_id']) ? "(<b>#{id}</b>) " : ''
        keywords = meme_data['keywords']

        text = "Meme <code>#{keywords}</code> #{meme_id}"\
               "confirmado por #{executer} (#{callback.from.id})."
        begin
            @tg.send_message(
                chat_id: meme_data['chat_id'],
                parse_mode: :html,
                reply_to_message_id: meme_data['message_id'],
                disable_web_page_preview: true,
                text: "Tu sugerencia de video ha sido <strong>ACEPTADA</strong>.\n\n"\
                      "<i> ¡Gracias por tu contribución con el bot! "\
                      "Para visualizarla visita @BibliotecaMeme.</i>\n\n"\
                      "• Puedes ayudarnos a mantener activo este proyecto 💖 con tu "\
                      "donativo en "\
                      '<a href="https://www.paypal.me/Alejandro95uy">PayPal</a>, '\
                      "no tienes ni idea de cuánto te lo agradeceríamos 🥺.\n\n\n"\
                      "• <i>Únete al</i> "\
                      '<a href="https://t.me/+svRw6oYW7yA4Njdh">Grupo de Difusión</a>.'
            )
        ensure
            @tg.edit_message_text(
                chat_id: callback.message.chat.id,
                parse_mode: :html,
                text: text,
                message_id: callback.message.message_id,
                disable_web_page_preview: true,
                disable_notification: true
            )

            @tg.send_video(
                chat_id: @channel_videos,
                parse_mode: :html,
                video: meme_data['video_id'],
                caption: "<i>#{keywords}</i>"
            )
        end
    end

    def confirm_new_meme_callback(temp_id, callback)
        meme_data = delete_temp_keys temp_id,
                                     callback.message.chat.id,
                                     callback.message.message_id

        meme_id = modify_database_newmeme(
            meme_data['keywords'],
            meme_data['video_id'],
            meme_data['message_id'],
            meme_data['user_id'],
            meme_data['chat_id']
        )

        check_group_keys meme_id, temp_id, meme_data['chat_id']

        meme_data['meme_id'] = meme_id
        meme_data
    end

    def delete_temp_keys(temp_id, chat_id, message_id)
        temp_key = "memes:temp:#{temp_id}"

        meme_data = @redis.hgetall(temp_key)
        @redis.del(temp_key)

        @redis.srem('memes:temp_videos', meme_data['video_id'])
        @redis.del "keyboard:#{chat_id}:#{message_id}"

        meme_data
    end

    def check_group_keys(meme_id, temp_id, chat_id)
        return unless @redis.srem("memes:temp_group:#{chat_id}", temp_id)

        @redis.sadd("memes:group:#{chat_id}", meme_id)
    end
end
