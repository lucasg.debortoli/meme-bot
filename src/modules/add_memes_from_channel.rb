class MemeBot
    add_handler Handler::Message.new(
        :add_memes_from_channel,
        allowed_chats: %w[private channel],
        types: %w[video]
    )

    def add_memes_from_channel(msg)
        return unless msg.chat.id == @channel_memes
        return unless msg.caption

        add_meme(msg, msg.video, msg.caption)
    end
end
