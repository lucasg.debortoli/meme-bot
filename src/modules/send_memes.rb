class MemeBot
    add_handler Handler::InlineQuery.new(:inline_memes)

    def inline_memes(query)
        @logger.info("Inline query: #{query}")

        return send_random_memes(query.id) if query.query.empty?
        return if query.query.size < 4

        keywords = parse_keywords(query.query)

        # arreglo de [item, distancia], ordenado según prioridad
        memes = []
        @redis.smembers('memes').each do |item|
            distance = closeness(keywords, item)

            next if distance > 2

            get_matching_memes(item) do |meme|
                memes << [meme, distance]
            end
        end

        memes.sort! { |a, b| a.last <=> b.last }
        memes = memes[0..49]
        list_of_memes = memes.map(&:first)

        memes.map!.with_index { |meme, i| create_inline_video_result(meme.first, i) }
        answer_query(query.id, memes, list_of_memes)
    end

    private

    def get_matching_memes(keywords)
        @redis.smembers("keywords_match:#{keywords}").each do |meme_id|
            yield @redis.hgetall("meme:#{meme_id}").transform_keys!(&:to_sym)
        end
    end

    def send_random_memes(query_id)
        if (meme_set = @redis.smembers('memes')).empty?
            return @tg.answerInlineQuery(
                inline_query_id: query_id,
                results: [],
                cache_time: 0
            )
        end

        memes = []
        meme_set.each do |item|
            get_matching_memes(item) { |meme| memes << meme }
        end

        memes = memes.sample(50)
        list_of_memes = memes.clone

        memes.map!.with_index { |meme, i| create_inline_video_result(meme, i) }
        answer_query(query_id, memes, list_of_memes)
    end

    def create_inline_video_result(meme, id)
        Telegram::Bot::Types::InlineQueryResultCachedVideo.new(
            type: 'video',
            id: id,
            video_file_id: meme[:video_id],
            title: meme[:keywords]
        )
    end

    # Levenshtein para substrings como https://gist.github.com/tomstuart/9e4fd5cd96527debf7a685d0b5399635
    def closeness(substring, big_string)
        distances = Array.new(big_string.size.succ, 0)

        substring.each_char.with_index do |sub_c, sub_i|
            next_distances = [sub_i.succ]

            big_string.each_char.with_index do |big_c, big_i|
                deletion = distances[big_i.succ].succ
                insertion = next_distances[big_i].succ
                substitution = distances[big_i] + (sub_c == big_c ? 0 : 1)

                next_distances << [deletion, insertion, substitution].min
            end

            distances = next_distances
        end

        distances.min
    end

    def answer_query(query_id, memes, list_of_memes)
        @logger.info("Cantidad de memes a enviar: #{memes.length}")

        begin
            @tg.answerInlineQuery(inline_query_id: query_id, results: memes,
                                  cache_time: 0)
        rescue Telegram::Bot::Exceptions::ResponseError => e
            case e.to_s
            when /VIDEO_CONTENT_TYPE_INVALID/
                list = (list_of_memes.map { |meme| meme[:keywords] }).join("\n")

                @logger.info(
                    'Alguno de estos videos hace saltar el error '\
                    "VIDEO_CONTENT_TYPE_INVALID:\n\n#{list}\n\n"\
                    'Hay que revisar cuál es y eliminarlo.',
                    to_channel: true
                )
            end
        end
    end
end
