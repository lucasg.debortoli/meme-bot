class MemeBot
    add_handler Handler::Command.new(
        :sugerencia,
        :suggestion,
        allow_params: true,
        description: 'Envía una sugerencia al admin del bot'
    )

    add_handler Handler::Command.new(
        :cancelarsugerencia,
        :cancelsuggestion,
        description: 'Cancela una sugerencia'
    )

    add_handler Handler::Message.new(
        :suggestion_response,
        allowed_chats: %w[private channel],
        types: %w[text]
    )

    def suggestion(msg, params)
        return unless validate_params_support msg, params

        name_user = user_link(msg.from, msg.chat.id) || 'eliminado'
        date = Time.at(msg.date, in: @tz.utc_offset)

        text = date.strftime("<code>[%d/%m/%Y %T]</code>\n")
        text << "#{name_user} (#{msg.from.id}) en "\
                "#{get_chat_title msg} (#{msg.chat.id}) "\
                "envía la siguiente sugerencia:\n\n#{html_parser params}\n\n"\
                'Respondiéndole a este mensaje se responderá a la sugerencia, '\
                'se le puede responder cualquier cantidad de veces. Para evitar '\
                "eso, responder a este mensaje con /cancelarsugerencia\n\n"\
                "Si esta persona está siendo molesta se puede bloquear del bot:\n"\
                "<code>/bloquear #{msg.from.id}</code>"

        result = @tg.send_message(
            chat_id: @owner,
            parse_mode: :html,
            disable_web_page_preview: true,
            disable_notification: true,
            text: text
        )

        return unless result && result['ok']

        after_return result['result'], msg
    end

    def suggestion_response(msg)
        return unless msg.chat.id == @owner
        return unless msg.text && msg.reply_to_message

        data = @redis.hgetall(
            "suggestions:#{msg.chat.id}:#{msg.reply_to_message.message_id}"
        )

        return if data.empty?

        @tg.send_message(
            chat_id: data['chat_id'],
            reply_to_message_id: data['message_id'],
            text: msg.text
        )

        @tg.send_message(
            chat_id: msg.chat.id,
            reply_to_message_id: msg.message_id,
            text: 'Sugerencia respondida'
        )
    end

    def cancelsuggestion(msg)
        return unless authorized(msg, action: 'usar este comando')

        unless msg.reply_to_message
            @tg.send_message(
                chat_id: msg.chat.id,
                reply_to_message_id: msg.message_id,
                text: 'Tienes que responder a un mensaje'
            )
            return
        end

        clave = "suggestions:#{msg.chat.id}:#{msg.reply_to_message.message_id}"
        data = @redis.hgetall clave

        if data.empty?
            @tg.send_message(
                chat_id: msg.chat.id,
                reply_to_message_id: msg.message_id,
                text: 'Ese mensaje no es de una sugerencia válida'
            )
            return
        end

        @redis.del clave

        @tg.send_message(
            chat_id: data['chat_id'],
            reply_to_message_id: data['message_id'],
            parse_mode: :html,
            text: '<strong>Esta sugerencia fue cancelada</strong>'
        )

        @tg.send_message(
            chat_id: msg.chat.id,
            reply_to_message_id: msg.message_id,
            text: "Sugerencia cancelada"
        )
    end

    private

    def after_return(result, msg)
        @redis.mapped_hmset(
            "suggestions:#{result['chat']['id']}:#{result['message_id']}",
            { chat_id: msg.chat.id, message_id: msg.message_id }
        )

        @tg.send_message(
            chat_id: msg.chat.id,
            reply_to_message_id: msg.message_id,
            text: 'Su sugerencia ha sido enviada con éxito'
        )
    end

    def validate_params_support(msg, params)
        if params.nil?
            @tg.send_message(
                chat_id: msg.chat.id,
                reply_to_message_id: msg.message_id,
                parse_mode: :html,
                text: "<strong>¡Bienvenido a las sugerencias del bot!</strong>\n\n"\
                      "Para enviar un mensaje al admin del bot debes enviar:\n\n"\
                      '<code>/sugerencia "mensaje a enviar"</code>'
            )
            return false
        elsif params.length < 5
            @tg.send_message(
                chat_id: msg.chat.id,
                reply_to_message_id: msg.message_id,
                text: 'Mensaje muy corto'
            )
            return false
        elsif params.length > 1000
            @tg.send_message(
                chat_id: msg.chat.id,
                reply_to_message_id: msg.message_id,
                text: 'Mensaje muy largo, máximo 1000 caracteres'
            )
            return false
        end
        true
    end
end
