class MemeBot
    add_handler Handler::Command.new(
        :borrarmeme,
        :delmeme,
        allow_params: true,
        description: 'Elimina un meme.'
    )

    def delmeme(msg, params)
        return unless authorized(msg, action: 'usar este comando')

        if params.nil? || params.empty?
            @tg.send_message(
                chat_id: msg.chat.id,
                text: 'Necesito que me pases una ID para buscar'
            )
            return
        end

        unless @redis.exists?("meme:#{params}")
            @tg.send_message(
                chat_id: msg.chat.id,
                text: 'No hay ningún meme con esa ID'
            )
            return
        end

        keywords = modify_database_delmeme(params)

        @tg.send_message(
            chat_id: msg.chat.id,
            reply_to_message_id: msg.message_id,
            parse_mode: :html,
            text: "El meme <b>#{keywords}</b> (<code>#{params}</code>) "\
                  'ha sido borrado con éxito'
        )

        @logger.info(
            "Meme <b>#{keywords}</b> (<code>#{params}</code>) borrado por "\
            "#{user_link msg.from, msg.chat} (#{msg.from.id})",
            parse_html: false,
            to_channel: true
        )
    end

    private

    def modify_database_delmeme(meme_id)
        keywords = @redis.hget("meme:#{meme_id}", 'keywords')
        @redis.del("meme:#{meme_id}")

        @redis.srem("keywords_match:#{keywords}", meme_id)
        
        if @redis.scard("keywords_match:#{keywords}").zero?
            @redis.del("keywords_match:#{keywords}")
            @redis.srem('memes', keywords)
        end
        
        keywords
    end
end
