class MemeBot
    add_handler Handler::Command.new(
        :estadisticas,
        :stats,
        description: 'Mando estadísticas del uso del bot'
    )

    def stats(msg)
        return unless authorized(msg, action: 'usar este comando')

        privates = @redis.scard 'chats:private'
        channels = @redis.scard 'chats:channel'
        groups = @redis.scard 'chats:group'
        supergroups = @redis.scard 'chats:supergroup'

        text = "<b><u>Estadísticas del bot</u></b>\n\n"\
               "<b>Chats activos</b>\n"\
               " - Privados: <code>#{privates}</code>\n"\
               " - Grupos: <code>#{groups}</code>\n"\
               " - Supergrupos: <code>#{supergroups}</code>\n"\
               " - Canales: <code>#{channels}</code>\n"

        @tg.send_message(
            chat_id: msg.chat.id,
            parse_mode: :html,
            text: text
        )
    end
end
