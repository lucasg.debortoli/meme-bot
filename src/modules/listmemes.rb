class MemeBot
    add_handler Handler::Command.new(
        :listarmemes,
        :list_memes,
        allow_params: true,
        description: 'Lista de memes cargados.'
    )

    def list_memes(msg, params)
        return unless authorized(msg, action: 'usar este comando')

        return unless (keywords = list_memes_validate_keywords(params, msg.chat.id))

        meme_set = @redis.smembers('memes').filter { |meme| meme[keywords] }

        if meme_set.empty?
            text = if params.nil?
                       'No hay memes, añada un video con /newmeme.'
                   else
                       'No hay memes cargados con ese nombre.'
                   end

            @tg.send_message(
                chat_id: msg.chat.id,
                text: text
            )
            return
        end

        arr = create_arr_listmemes meme_set
        options = armar_botonera(0, arr.size, msg.from.id)

        resp = @tg.send_message(chat_id: msg.chat.id,
                                parse_mode: :html,
                                reply_markup: options,
                                text: arr.first,
                                disable_notification: true)

        return unless resp

        armar_lista(msg.chat.id,
                    Telegram::Bot::Types::Message.new(resp['result']).message_id,
                    arr)
    end

    private

    def list_memes_validate_keywords(params, chat_id)
        keywords = parse_keywords(params || '')

        if !params.nil? && keywords.empty?
            @tg.send_message(
                chat_id: chat_id,
                text: 'Parámetros inválidos.'
            )
            return nil
        end

        keywords
    end

    def create_arr_listmemes(meme_keyword_set)
        title = "Lista de memes\n\n"

        arr = []

        add_line = proc do |item|
            "\n#{item[:keywords]}: <code>#{item[:meme_id]}</code>"
        end
        memes = meme_keyword_set.map do |keywords|
            @redis.smembers("keywords_match:#{keywords}").map do |meme_id|
                { keywords: keywords, meme_id: meme_id }
            end
        end
        memes.flatten!

        board_array(
            arr: arr,
            title: title,
            counter: 0,
            max_lines: 30,
            max_size: 1000,
            code_block: add_line,
            iterable_set: memes.sort_by { |hash| hash[:keywords] },
            subtitle: "IDs asociadas a un nombre.\n"
        )
        arr
    end
end
