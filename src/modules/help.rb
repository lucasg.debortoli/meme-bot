class MemeBot
    add_handler Handler::Command.new(:help, :help)

    add_handler Handler::Command.new(
        :ayuda,
        :help,
        description: 'Mando el mensaje de ayuda'
    )

    def help(msg)
        text = "Soy @#{user.username}\n"\
               "Mis comandos disponibles son:\n\n"

        if AUTORITIES.include? msg.from.id
            self.class.commands.each do |command, handler|
                text << "/#{command} - #{handler.description}\n" if handler.description
            end
        else
            text << "<code>/inicio</code> - Iniciar el bot\n"\
                    "<code>/ayuda</code> - Lista de comandos\n"\
                    "<code>/nuevomeme</code> - Envía nueva sugerencia de video meme\n"\
                    '<code>/sugerencia</code> - Envía un mensaje al admin del bot'
        end

        @tg.send_message(
            chat_id: msg.chat.id,
            parse_mode: :html,
            text: text
        )
    end
end
