class MemeBot
    add_handler Handler::CallbackQuery.new(:callback_cancel_custom, 'cancel_custom')

    def callback_cancel_custom(callback)
        return unless authorized(callback, callback: true, action: 'apretar ese botón',
                                           lower_perm: true)
        return unless (match = callback.data.match(/\Acancel_custom:(?<temp_id>\d+)\z/))

        chat_id = callback.message.chat.id
        message_id = callback.message.message_id

        return unless can_press_keyboard?(
            callback.id,
            chat_id,
            message_id,
            'custom_rejection',
            'Este tablero ya fue utilizado, si siguen apareciendo los botones es '\
            'porque hubo un error de telegram al editar ese mensaje.'
        )

        temp_id = match['temp_id']
        return if self_callback_request(callback, temp_id)

        temp_id = @redis.get "keyboard:temp_id:#{chat_id}:#{message_id}"
        @redis.del "keyboard:temp_id:#{chat_id}:#{message_id}"

        send_reject_options temp_id, callback
    end

    private

    #### Send this keyboard ####

    def send_custom_reject(temp_id, callback)
        arr = [[
            Telegram::Bot::Types::InlineKeyboardButton.new(
                text: 'Cancelar',
                callback_data: "cancel_custom:#{temp_id}"
            )
        ]]
        options = Telegram::Bot::Types::InlineKeyboardMarkup.new inline_keyboard: arr
        chat_id = callback.message.chat.id
        message_id = callback.message.message_id

        @tg.edit_message_text(
            chat_id: chat_id,
            message_id: message_id,
            text: 'Responde este mensaje (no al video) con la razón personalizada',
            reply_markup: options,
            disable_web_page_preview: true,
            disable_notification: true
        )

        @redis.set "keyboard:#{chat_id}:#{message_id}", 'custom_rejection'
        @redis.set "keyboard:temp_id:#{chat_id}:#{message_id}", temp_id
    end
end
