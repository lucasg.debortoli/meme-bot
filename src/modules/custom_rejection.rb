class MemeBot
    add_handler Handler::Message.new(
        :custom_rejection,
        allowed_chats: %w[private channel],
        types: %w[text]
    )

    def custom_rejection(msg)
        return unless msg.chat.id == @channel_logging
        return unless msg.text && msg.reply_to_message

        chat_id = msg.chat.id
        message_id = msg.reply_to_message.message_id
        return if @redis.get("keyboard:#{chat_id}:#{message_id}") != 'custom_rejection'

        temp_id = @redis.get "keyboard:temp_id:#{chat_id}:#{message_id}"
        meme_data = delete_temp_keys temp_id, chat_id, message_id

        origin_chat_id = meme_data['chat_id']
        @redis.srem("memes:temp_group:#{origin_chat_id}", temp_id)

        begin
            keywords = html_parser meme_data['keywords']
            reject_msg = "Meme <code>#{keywords}</code> "\
                         'rechazado: '

            @tg.send_message(
                chat_id: origin_chat_id,
                reply_to_message_id: meme_data['message_id'],
                text: reject_msg + msg.text[0..(4095 - reject_msg.length)],
                parse_mode: :html
            )

            @tg.send_message(
                chat_id: msg.chat.id,
                reply_to_message_id: msg.message_id,
                text: 'Mensaje de rechazo enviado'
            )
        ensure
            delete_keyboard_keys_and_edit_message chat_id, message_id, msg, keywords
        end
    end

    private

    def delete_keyboard_keys_and_edit_message(chat_id, message_id, msg, keywords)
        @redis.del("keyboard:#{chat_id}:#{message_id}")
        @redis.del("keyboard:temp_id:#{chat_id}:#{message_id}")

        reject = msg.from ? "por #{user_link msg.from, msg.chat} (#{msg.from.id})" : ''

        @tg.edit_message_text(
            chat_id: chat_id,
            message_id: message_id,
            text: "Meme <code>#{keywords}</code> rechazado #{reject}",
            parse_mode: :html,
            disable_web_page_preview: true,
            disable_notification: true
        )
    end
end
