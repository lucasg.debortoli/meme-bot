class MemeBot
    add_handler Handler::CallbackQuery.new(:callback_reject_options, 'reject_options')

    def callback_reject_options(callback)
        return unless authorized(callback, callback: true, action: 'apretar ese botón',
                                           lower_perm: true)

        match = callback.data.match(/\Areject_options:(?<temp_id>\d+):(?<action>.+)\z/)

        return unless can_press_keyboard?(
            callback.id,
            callback.message.chat.id,
            callback.message.message_id,
            'reject_options',
            'Este tablero ya fue utilizado, si siguen apareciendo los botones es '\
            'porque hubo un error de telegram al editar ese mensaje.'
        )

        temp_id = match['temp_id']
        return if self_callback_request(callback, temp_id)

        case match['action']
        when 'none'
            answer_message_rejection callback, temp_id
        when 'custom'
            send_custom_reject temp_id, callback
        when 'back'
            keywords = @redis.hget "memes:temp:#{temp_id}", 'keywords'
            back_to_callback_confirm_reject temp_id, keywords, callback
        end
    rescue Telegram::Bot::Exceptions::ResponseError => e
        @logger.warn e.to_s, to_channel: true
    end

    private

    #### Send keyboard reject ####

    def send_reject_options(temp_id, callback)
        options = Telegram::Bot::Types::InlineKeyboardMarkup.new inline_keyboard: [
            [
                Telegram::Bot::Types::InlineKeyboardButton.new(
                    text: 'Ninguna',
                    callback_data: "reject_options:#{temp_id}:none"
                ),
                Telegram::Bot::Types::InlineKeyboardButton.new(
                    text: 'Personalizada',
                    callback_data: "reject_options:#{temp_id}:custom"
                )
            ],
            [
                Telegram::Bot::Types::InlineKeyboardButton.new(
                    text: 'Volver',
                    callback_data: "reject_options:#{temp_id}:back"
                )
            ]
        ]

        chat_id = callback.message.chat.id
        message_id = callback.message.message_id

        @tg.edit_message_text(
            chat_id: chat_id,
            message_id: message_id,
            text: '¿Por qué razón?',
            reply_markup: options,
            disable_web_page_preview: true,
            disable_notification: true
        )

        @redis.set(
            "keyboard:#{chat_id}:#{message_id}",
            'reject_options'
        )
    end

    #### Aux callback ####

    def back_to_callback_confirm_reject(temp_id, keywords, callback)
        arr = [
            [
                Telegram::Bot::Types::InlineKeyboardButton.new(
                    text: 'Aceptar',
                    callback_data: "new_meme:#{temp_id}:confirm"
                ),
                Telegram::Bot::Types::InlineKeyboardButton.new(
                    text: 'Rechazar',
                    callback_data: "new_meme:#{temp_id}:reject"
                )
            ],
            [
                Telegram::Bot::Types::InlineKeyboardButton.new(
                    text: 'Bloquear y borrar',
                    callback_data: "new_meme:#{temp_id}:ban"
                )
            ]
        ]

        options = Telegram::Bot::Types::InlineKeyboardMarkup.new inline_keyboard: arr
        chat_id = callback.message.chat.id
        message_id = callback.message.message_id

        @tg.edit_message_text(
            chat_id: chat_id,
            message_id: message_id,
            parse_mode: :html,
            text: "¿Aceptar el meme <code>#{keywords}</code>?",
            reply_markup: options,
            disable_web_page_preview: true,
            disable_notification: true
        )

        @redis.set "keyboard:#{chat_id}:#{message_id}", 'to_press'
    end

    def answer_message_rejection(callback, temp_id)
        chat_id = callback.message.chat.id
        message_id = callback.message.message_id

        meme_data = delete_temp_keys temp_id, chat_id, message_id
        origin_chat_id = meme_data['chat_id']
        @redis.srem("memes:temp_group:#{origin_chat_id}", temp_id)

        begin
            @tg.send_message(
                chat_id: meme_data['chat_id'],
                reply_to_message_id: meme_data['message_id'],
                parse_mode: :html,
                disable_web_page_preview: true,
                text: 'Hemos evaluado su sugerencia de video '\
                      "y hemos decidido <strong>NO ACEPTARLA</strong>.\n\n"\
                      'En estos momentos, no podemos ofrecer motivos concretos '\
                      'sobre el rechazo, sin embargo puedes ver los requisitos '\
                      "que se solicitan para un Video Meme.\n\n"\
                      'Y si tienes alguna sugerencia o quieras comunicarte con '\
                      "el admin del bot usa el comando:\n\n"\
                      "<code>/sugerencia  \"mensaje a enviar\"</code>\n\n"\
                      "O también puede ingresar al grupo de bot\n"\
                      "<a href=\"https://t.me/+R7SJyYv3NsXVxuK_\">"\
                      "&gt;&gt; Click aquí &lt;&lt;</a>"
            )
        ensure
            @redis.del("keyboard:#{chat_id}:#{message_id}")

            @tg.edit_message_text(
                chat_id: chat_id,
                message_id: message_id,
                parse_mode: :html,
                disable_web_page_preview: true,
                disable_notification: true,
                text: "Meme <code>#{meme_data['keywords']}</code> rechazado "\
                      "por #{user_link callback.from, callback.message.chat} "\
                      "(#{callback.from.id})"
            )
        end
    end
end
