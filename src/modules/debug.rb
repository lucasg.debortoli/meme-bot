class MemeBot
    add_handler Handler::Command.new(:debug, :debug)

    def debug(msg)
        return unless AUTORITIES.include?(msg.from.id)

        @tg.send_message(
            chat_id: msg.chat.id,
            text: pretty_debug(msg.raw_data),
            disable_web_page_preview: true,
            disable_notification: true
        )
    end

    private

    def pretty_debug(raw_data)
        text = "{\n"
        ajust_text_object!(raw_data, text, 1)
        text << '}'
        text
    end

    def ajust_text_object!(object, text, deep)
        tabs = ' ' * 4 * deep

        case object
        when Hash
            adjust_text_debug_dicc(object, text, deep, tabs)
        when Array
            adjust_text_debug_array(object, text, deep, tabs)
        end
    end

    def adjust_text_debug_dicc(object, text, deep, tabs)
        return if empty_object object, text, '{}'

        object.each_with_index do |(key, value), índice|
            text << "#{tabs}\"#{key}\": "

            case value
            when Hash
                text << "{\n"
                ajust_text_object! value, text, deep + 1
                text << "#{tabs}}"
            when Array
                text << "[\n"
                ajust_text_object! value, text, deep + 1
                text << "#{tabs}]"
            else
                text << "\"#{value}\""
            end

            add_separator text, índice, object.size
        end
    end

    def adjust_text_debug_array(object, text, deep, tabs)
        return if empty_object object, text, '[]'

        object.each_with_index do |elem, i|
            case elem
            when Hash
                text << "#{tabs}{\n"
                ajust_text_object! elem, text, deep + 1
                text << "#{tabs}}"
            when Array
                text << "#{tabs}[\n"
                ajust_text_object! elem, text, deep + 1
                text << "#{tabs}]"
            else
                text << "#{tabs}\"#{elem}\""
            end

            add_separator text, i, object.size
        end
    end

    def empty_object(object, text, vacío)
        text << vacío if (es_vacío = object.empty?)
        es_vacío
    end

    def add_separator(text, index_separator, size)
        text << ',' if index_separator != size - 1
        text << "\n"
    end
end
