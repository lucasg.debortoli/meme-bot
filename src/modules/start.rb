class MemeBot
    add_handler Handler::Command.new(:start, :start)

    add_handler Handler::Command.new(
        :inicio,
        :start,
        description: 'Mando el mensaje de inicio'
    )

    def start(msg)
        @tg.send_message(
            chat_id: msg.chat.id,
            parse_mode: :html,
            text: "<strong>¡Hola Bienvenido a VidMeme!</strong>\n\n"\
                  'Soy un bot para mandar videomemes a cualquier chat de forma '\
                  'inline, sólo escribe @VidMemebot seguido de la frase del meme, '\
                  "también puedes agregar tus videomemes a través de este bot. 🤖\n\n"\
                  'Para agregar tus videomemes, manda el videomeme que quieres '\
                  'agregar al bot, este tiene que tener una duración máxima de '\
                  "10 segundos, responde a tu vídeo con el comando:\n\n "\
                  "<code>/nuevomeme + la frase con la que quieres guardarlo</code>\n\n"\
                  "Ejemplo:\n\n"\
                  "<code>/nuevomeme Auronplay este es tu ídolo?</code>\n\n"\
                  "Para conocer los videos que ya tiene el bot consulte "\
                  "el canal @BibliotecaMeme\n\n"\
                  '<strong>PD</strong>: <i>El bot es de videomemes que pueden ser '\
                  'usados como respuesta en un chat, no para mandar vídeos random '\
                  'o de humor.</i>'
        )
    end
end
