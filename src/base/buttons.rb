class MemeBot
    def edit_button_list(callback)
        match = callback.data.match(/list:(?<user_id>\d+):(?<index>\d+)/)

        user_id = match[:user_id].to_i
        chat_id = callback.message.chat.id
        message_id = callback.message.message_id
        index = match[:index].to_i

        metadata = get_metadata_list(chat_id, message_id)

        return if invalid_user(user_id, callback, metadata)

        options = armar_botonera(
            index,
            get_list_size(chat_id, message_id),
            user_id
        )

        value = get_list_element(
            chat_id,
            message_id,
            index
        )

        @tg.answer_callback_query(callback_query_id: callback.id)
        return if value_nil?(value, chat_id, message_id)
        return if current_page_list(callback.message, value, metadata)

        edit_buttons(metadata, chat_id, message_id, value, options)
    rescue Telegram::Bot::Exceptions::ResponseError => e
        @logger.error e.to_s, to_channel: true
    end

    def invalid_user(user_id, callback, metadata)
        if user_id != callback.from.id && metadata[:editable_by] == 'owner'
            @tg.answer_callback_query(callback_query_id: callback.id,
                                      text: 'Esta no es tu lista')
            return true
        end
        false
    end

    def value_nil?(value, chat_id, message_id)
        if value.nil?
            @tg.edit_message_text(
                chat_id: chat_id,
                message_id: message_id,
                text: 'Esta lista es muy vieja, pide una lista nueva.'
            )
            return true
        end
        false
    end

    def edit_buttons(metadata, chat_id, message_id, value, options)
        case metadata[:type]
        when 'text'
            @tg.edit_message_text(chat_id: chat_id,
                                  parse_mode: :html,
                                  message_id: message_id,
                                  disable_web_page_preview: true,
                                  disable_notification: true,
                                  text: value,
                                  reply_markup: options)
        when 'caption'
            @tg.edit_message_caption(chat_id: chat_id,
                                     message_id: message_id,
                                     parse_mode: :html,
                                     caption: value,
                                     reply_markup: options)
        else
            @tg.edit_message_media(chat_id: chat_id, message_id: message_id,
                                   media: { type: metadata[:type],
                                            media: value }.to_json,
                                   reply_markup: options)
        end
    end

    def acciones_inferiores_lista(callback)
        match = callback.data.match(
            /\Aoptionslist:(?<user_id>\d+):(?<index>\d+)(:(?<acción>\w+))?\z/
        )

        user_id = match[:user_id].to_i
        chat_id = callback.message.chat.id
        message_id = callback.message.message_id
        index = match[:index].to_i

        # valido user_id y que sea editable
        if user_id != callback.from.id
            @tg.answer_callback_query(callback_query_id: callback.id,
                                      text: 'No puedes hacer eso')
            return
        end

        do_action(
            match: match,
            callback: callback,
            chat_id: chat_id,
            message_id: message_id,
            index: index,
            user_id: user_id
        )
    rescue Telegram::Bot::Exceptions::ResponseError => e
        @logger.error e.to_s, to_channel: true
    end

    def do_action(params)
        callback = params[:callback]
        chat_id = params[:chat_id]
        message_id = params[:message_id]
        index = params[:index]
        user_id = params[:user_id]

        case params[:match][:acción]
        when 'borrar'
            @tg.answer_callback_query(callback_query_id: callback.id,
                                      text: 'Mensaje borrado.')
            @tg.delete_message(chat_id: chat_id, message_id: message_id)
        when 'edit'
            @redis.hset("botonera:#{chat_id}:#{message_id}:metadata",
                        'editable_by', 'all')
            @tg.answer_callback_query(callback_query_id: callback.id,
                                      text: 'Botonera ahora es presionable por todos.')
            options = armar_botonera(index, get_list_size(chat_id, message_id),
                                     user_id)
            @tg.edit_message_reply_markup(chat_id: chat_id, message_id: message_id,
                                          reply_markup: options)
        when 'noedit'
            @redis.hset("botonera:#{chat_id}:#{message_id}:metadata",
                        'editable_by', 'owner')
            @tg.answer_callback_query(callback_query_id: callback.id,
                                      text: 'Botonera ahora solo es presionable '\
                                            'por el que la pidió.')
            options = armar_botonera(index, get_list_size(chat_id, message_id),
                                     user_id)
            @tg.edit_message_reply_markup(chat_id: chat_id, message_id: message_id,
                                          reply_markup: options)
        end
    end

    # Guarda el arreglo en redis, type puede valer 'texto', 'photo', 'video',
    # 'animation', 'audio', 'document' o 'caption'
    # editable puede valer 'owner' o 'all'. son los que pueden tocar los  botones
    def armar_lista(chat_id, id_msg, arreglo, type = 'text', editable = 'owner')
        clave = "botonera:#{chat_id}:#{id_msg}"
        @redis.rpush clave, arreglo
        @redis.mapped_hmset "#{clave}:metadata", type: type, editable_by: editable
        # 86400 = 24*60*60 -> un día en segundos
        @redis.expire clave, 86_400
        @redis.expire "#{clave}:metadata", 86_400
    end

    def get_list_element(chat_id, id_msg, index)
        @redis.lindex "botonera:#{chat_id}:#{id_msg}", index
    end

    def get_list_size(chat_id, id_msg)
        @redis.llen "botonera:#{chat_id}:#{id_msg}"
    end

    def get_metadata_list(chat_id, id_msg)
        h = @redis.hgetall("botonera:#{chat_id}:#{id_msg}:metadata")
        h.transform_keys!(&:to_sym)
    end

    def armar_botonera(página_actual, tamaño_máximo, user_id)
        return nil if tamaño_máximo == 1

        página_actual = [página_actual, tamaño_máximo - 1].min # valido el rango

        arr = [[]]

        if tamaño_máximo <= 5
            tamaño_máximo.times do |i|
                arr.first << Telegram::Bot::Types::InlineKeyboardButton.new(
                    text: página_actual == i ? "< #{i + 1} >" : (i + 1).to_s,
                    callback_data: "list:#{user_id}:#{i}"
                )
            end
            return Telegram::Bot::Types::InlineKeyboardMarkup.new inline_keyboard: arr
        end

        botones = []

        definir_botones(botones, página_actual, tamaño_máximo)

        botones.each do |botón|
            arr.first << Telegram::Bot::Types::InlineKeyboardButton.new(
                text: botón.first,
                callback_data: "list:#{user_id}:#{botón.last}"
            )
        end

        Telegram::Bot::Types::InlineKeyboardMarkup.new(inline_keyboard: arr)
    end

    def definir_botones(botones, página_actual, tamaño_máximo)
        if página_actual < 3
            4.times do |i|
                botones << [página_actual == i ? "<#{i + 1}>" : (i + 1).to_s, i]
            end
            botones << ["#{tamaño_máximo} >>", tamaño_máximo - 1]
        elsif página_actual > (tamaño_máximo - 4)
            botones << ['<< 1', 0]
            ((tamaño_máximo - 4)..(tamaño_máximo - 1)).each do |i|
                botones << [página_actual == i ? "<#{i + 1}>" : (i + 1).to_s, i]
            end
        else
            botones << ['<< 1', 0]
            botones << ["< #{página_actual}", página_actual - 1]
            botones << ["< #{página_actual + 1} >", página_actual]
            botones << ["#{página_actual + 2} >", página_actual + 1]
            botones << ["#{tamaño_máximo} >>", tamaño_máximo - 1]
        end
    end

    # Método que manda una botonera preguntando si se quiere enviar un nsfw.
    # Devuelve el id del mensaje, o false si no pudo enviar.
    # Uno tiene que hacer su propio handler con el prefijo pasado.
    def preguntar_nsfw(chat_id, user_id, prefijo_callback)
        arr = %w[Mostrar Borrar]
        arr.map! do |botón|
            Telegram::Bot::Types::InlineKeyboardButton.new(
                text: botón,
                callback_data: "#{prefijo_callback}:#{user_id}:#{botón}"
            )
        end

        botones = Telegram::Bot::Types::InlineKeyboardMarkup.new(inline_keyboard: [arr])
        respuesta = @tg.send_photo(chat_id: chat_id,
                                   caption: 'El contenido que querés ver es NSFW. '\
                                            '¿Ver de todas formas?',
                                   photo: 'https://i.imgur.com/I7buUmr.jpg',
                                   reply_markup: botones)
        return false unless respuesta

        respuesta = Telegram::Bot::Types::Message.new respuesta['result']

        respuesta.message_id
    end

    def current_page_list(msg, new_val, metadata)
        case metadata[:type]
        when 'text'
            new_val.gsub(%r{</?[a-z]+>}, '') == msg.text
        else
            @logger.warn "implementar chequeo de página para #{metadata[:type]}"
            false
        end
    end
end
