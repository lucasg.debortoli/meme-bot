require_relative 'version'
require_relative 'handlers'
require_relative 'logger'
require_relative 'telegram'
require_relative 'buttons'
require_relative 'exceptions'
require_relative 'memebot_aux'

require 'redis'
require 'tzinfo'
require 'set'

class MemeBot
    attr_reader :tg, :logger, :redis, :user

    AUTORITIES = Set.new(
        [
            240_524_686, # Luke
            267_832_653, # Galerazo
            327_078_036 # Alejandro
        ]
    ).freeze

    LOWER_PERMISSIONS = Set.new(
        [
            311_798_935
        ]
    )

    def self.add_handler(handler)
        case handler
        when Handler::Command
            @commands ||= {}
            @commands[handler.cmd] = handler
        when Handler::InlineQuery
            @inlinequery ||= handler
        when Handler::CallbackQuery
            @callbacksquery ||= []
            @callbacksquery << handler
        else
            @handlers ||= []
            @handlers << handler
        end
    end

    def self.handlers
        @handlers ||= []
    end

    def self.commands
        @commands ||= {}
    end

    def self.inlinequery
        @inlinequery ||= nil
    end

    def self.callbacksquery
        @callbacksquery ||= []
    end

    # args is a hash from config.yml
    def initialize(args)
        @channel_logging = args[:channel_logging].to_i
        @channel_memes = args[:channel_memes].to_i
        @channel_videos = args[:channel_videos].to_i
        @owner = args[:owner].to_i

        logger = Logger.new $stderr
        @logger = DankieLogger.new logger, @channel_logging
        @tg = TelegramAPI.new args[:tg_token], @logger
        @logger.initialize_client @tg

        @redis = Redis.new(port: args[:redis_port], host: args[:redis_host],
                           password: args[:redis_pass])

        @user = Telegram::Bot::Types::User.new @tg.get_me['result']
        @tz = TZInfo::Timezone.get args[:timezone]

        Telegram::Bot::Types::Base.attr_accessor :raw_data
    end

    def run
        @logger.info "Bot started"
        next_update = 0
        retries_tg_connection = 0

        loop do
            # Ciclo principal
            updates = @tg.get_updates(
                # Si la clave no existe, el .to_i convierte el nil en 0
                offset: next_update,
                timeout: 7,
                allowed_updates: %w[message edited_message channel_post
                    edited_channel_post inline_query chosen_inline_result callback_query
                    shipping_query pre_checkout_query poll poll_answer my_chat_member
                    chat_member].to_json
            )

            retries_tg_connection = 0
            next unless updates

            updates['result'].each do |update|
                upd_object = Telegram::Bot::Types::Update.new(update)
                @logger.info "Analizing update #{upd_object.update_id}"

                message = upd_object.current_message
                message.raw_data = update

                if message.is_a?(Telegram::Bot::Types::Poll)
                    @logger.info "Poll created: #{message}"
                    next
                end

                next if message&.from&.id &&
                        @redis.sismember('user_blacklist', message.from.id.to_s)
                next if message.respond_to?(:chat) &&
                        @redis.sismember('chat_blacklist', message.chat.id)

                begin
                    dispatch(message)
                rescue StandardError => e
                    killer_exception(e)
                end

                next_update = upd_object.update_id + 1
            end

        rescue Faraday::ConnectionFailed, Faraday::TimeoutError,
               HTTPClient::ReceiveTimeoutError, Net::OpenTimeout => e
            @logger.fatal "Conection Error: #{e.class}"
            retries_tg_connection += 1
            if retries_tg_connection >= 10
                logger.fatal(
                    "Error de conexión con la api de telegram: "\
                    "#{retries_tg_connection} intentos consecutivos fallidos",
                    to_channel: true
                )
            end

        rescue StandardError => e
            killer_exception(e)
            retry
        end
    end

    def dispatch(update)
        case update
        when Telegram::Bot::Types::CallbackQuery
            self.class.callbacksquery.each do |handler|
                handler.run(self, update) if handler.check(update)
            end

        when Telegram::Bot::Types::InlineQuery
            self.class.inlinequery.run(self, update)
        when Telegram::Bot::Types::Message
            self.class.handlers.each do |handler|
                handler.run(self, update) if handler.check(update)
            end

            self.class.commands[get_command(update)]&.run(self, update)
        end
    end

    def get_command(msg)
        cmd = _parse_command(msg)
        cmd[:command]
    end

    def get_command_params(msg)
        cmd = _parse_command(msg)
        cmd[:params]
    end

    private

    def killer_exception(exception)
        return if @tg.catch(exception)

        text, backtrace = @logger.exception_text(exception)
        @logger.fatal text, to_channel: true, backtrace: backtrace
    rescue StandardError => e
        @logger.fatal "EXCEPCIÓN: #{e}\nLEYENDO LA EXCEPCIÓN: #{exception}\n\n"\
                      "#{@logger.exception_text(e).last}", to_channel: true
    end

    def html_parser(text)
        CGI.escapeHTML(text)
    end

    def _parse_command(msg)
        no_command = { command: nil, params: nil }

        return no_command unless (text = msg.text || msg.caption)
        return no_command if text.size <= 1

        command = nil
        params = nil

        if text.start_with? '/' # "/cmd params" o "/cmd@bot params"
            command, params = text.split ' ', 2
            command.downcase!
            command.gsub!(%r{^/([a-z]+)(@#{@user.username.downcase})?}, '\\1')

        elsif ['!', '>', '$', '.'].include? text[0] # "!cmd params" o ">cmd params"
            command, params = text.split ' ', 2
            command = command[1..]
            command.downcase!
        else
            result = parse_contact_command(text, msg)
            command = result[:command]
            params = result[:params]
        end

        return no_command if command.nil?

        { command: command&.to_sym, params: params }
    end

    def parse_contact_command(text, msg)
        arr = text.split(' ', 3) # ["user", "command", "params"]
        arr.first.downcase!
        if (arr.size > 1) && arr.first.casecmp(@user.username[0..-4]).zero?
            command = arr[1].downcase.to_sym
            params = arr[2]
        # Responde al bot
        elsif msg.reply_to_message&.from&.id == @user.id
            command, params = text.split ' ', 2
            command.downcase!
        else
            command = nil
            params = nil
        end
        { command: command, params: params }
    end
end
