class ExceptionHandler
    def initialize(logger)
        @logger = logger
    end

    def log(exception, args)
        handle(exception, args)
    end

    def catch(exception)
        handle(exception, nil)
    end

    private

    def handle(exception, _args)
        return unless exception.respond_to?(:error_code) &&
                      exception.respond_to?(:message)

        method = "handle_error_#{exception.error_code}".to_sym
        send(method, exception.message) if respond_to?(method, true)
    end

    def handle_error_400(error_msg)
        handled = true
        case error_msg
        when /query is too old and response timeout expired or query ID is invalid/
            @logger.error('Tardé demasiado en responder a una query inline',
                          to_channel: true)
        when /have no rights to send a message/
            @logger.error('Me restringieron los mensajes y'\
                          "no puedo mandar nada:\n#{error_msg}")
        when /have no write access to the chat/
            @logger.error('Me restringieron y solo puedo mandar mensajes de texto '\
                          " y no puedo mandar nada.\n#{error_msg}")
        when /not enough rights to send (sticker|animation)s to the chat/
            @logger.error('Me restringieron los stickers y gifs:'\
                          "\n#{error_msg}")
        when /(?-x:not enough rights to send )
              (?-x:(photo|document|video|audio|v(oice|ideo) note)s to the chat)/x
            @logger.error('Me restringieron la multimedia'\
                          "\n#{error_msg}")
        when /PEER_ID_INVALID/
            @logger.fatal('Le quise mandar un mensaje privado'\
                          ' a alguien que no me habló primero o me bloqueó.',
                          to_channel: true)
            handled = false
        when /chat not found/
            @logger.fatal('Quise mandar un mensaje pero parece que el '\
                          'chat no existe o fue brutalmente DOMADO y ULTRAJADO '\
                          'por telegram', to_channel: true)
            handled = false
        else
            handled = false
        end
        handled
    end

    def handle_error_403(error_msg)
        handled = true

        case error_msg
        when /bot is not a member of the (super)?group chat/
            @logger.error('Error. Me fui del chat y no puedo mandar mensajes.'\
                          "\n#{error_msg}")
        when /bot is not a member of the channel chat/
            @logger.error('Error. Me fui o me sacaron los permisos de '\
                          "mandar mensaje en el canal.\n#{error_msg}")
        when /bot was kicked from the (super)?group chat/
            @logger.error('Error. Me echaron del char y no puedo '\
                          "mandar mensajes.\n#{error_msg}")
        when /bot can't send messages to bots/
            @logger.error('Error. No puedo hablar con otros '\
                          "bots.\n#{error_msg}")
            handled = false
        when /bot was blocked by the user/
            @logger.error("Error. Ese usuario me bloqueó.\n#{error_msg}")
        else
            handled = false
        end
        handled
    end

    def handle_error_429(error_msg)
        handled = true

        case error_msg
        when /Too Many Requests: retry after/
            seconds = error_msg.split('{"retry_after"=>').last.split('}').first
            @logger.error("Por #{seconds} segundos no puedo mandar mensajes "\
                          "\n#{error_msg}")
        else
            handled = false
        end
        handled
    end
end
