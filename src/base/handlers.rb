module Handler
    class Message
        MSG_TYPES = %i[text audio document game photo
                       sticker video voice video_note contact
                       location venue poll reply_markup].freeze

        def initialize(callback, args = {})
            @callback = callback
            @allow_edited = args[:allow_edited] || false
            @allowed_chats = args[:allowed_chats]&.map(&:to_s) ||
                             %w[private group supergroup] # channel es otra opción
            @types = args[:types] || MSG_TYPES
        end

        def check(msg)
            return if invalid_conditions(msg)

            @types.each do |type|
                type_msg = msg.send type
                return true if type_msg && !(type_msg.is_a?(Array) && type_msg.empty?)
            end

            false
        end

        def run(bot, msg)
            bot.public_send(@callback, msg)
        end

        private

        def invalid_conditions(msg)
            !@allowed_chats.include?(msg.chat.type) ||
                (!@allow_edited && msg.edit_date)
        end
    end

    class Command
        attr_reader :cmd, :description

        def initialize(cmd, callback, args = {})
            @cmd = cmd
            @callback = callback
            @description = args[:description]
            @allow_params = args[:allow_params] || false
            @allow_edited = args[:allow_edited] || false
            # 'channel' es otra opción
            @allowed_chats = args[:allowed_chats]&.map(&:to_s) ||
                             %w[private group supergroup]
        end

        def run(bot, msg)
            return unless msg.is_a? Telegram::Bot::Types::Message
            return if !@allow_edited && msg.edit_date

            unless @allowed_chats.include?(msg.chat.type)
                bot.invalid_chat(msg, @allowed_chats)
                return
            end

            return if @cmd != bot.get_command(msg)

            bot.logger.info "CommandHandler: command \"#{@cmd}\" in #{msg.chat.id}"

            if @allow_params
                bot.public_send(@callback, msg, bot.get_command_params(msg))
            else
                bot.public_send(@callback, msg)
            end
        end
    end

    class CallbackQuery
        def initialize(callback, clave)
            @callback = callback
            @clave = clave
            @cache = {}
        end

        def check(callback)
            callback.data.start_with?("#{@clave}:")
        end

        def run(bot, callback)
            bot.logger.info "CallbackQueryHandler: callback #{callback.data} "\
                            "en #{callback.message.chat.id}"
            bot.public_send(@callback, callback)
        end
    end

    # Inicializarlo con el type de atributess que querés que soporte el handler
    # (los posibles son los de msg_TYPES). Lo podés inicializar con un solo
    # elemento (como símbolo) o con una lista no vacía de elementos
    # (strings o símbolos). Si no le pasás ningún type, toma all los de msg_TYPES.
    class ChatEvent
        # migrate_to_chat_id NO ESTÁ porque decidimos ignorar los mensajes
        # que contengan ese campo (ya que van a ser los últimos que existan
        # en ese chat y pueden generar quilombetes). Para saber cuándo un
        # grupo migra a supergrupo dejamos migrate_from_chat_id
        MSG_TYPES = %i[new_chat_members left_chat_member new_chat_title
                       new_chat_photo delete_chat_photo group_chat_created
                       supergroup_chat_created channel_chat_created
                       migrate_from_chat_id pinned_message invoice
                       successful_payment connected_website passport_data].freeze

        def initialize(callback, args = {})
            @types = args[:types] || MSG_TYPES

            @types.each do |atributes|
                unless MSG_TYPES.include? atributes
                    raise "#{atributes} not a valid types"
                end
            end

            @allowed_chats = args[:allowed_chats]&.map(&:to_s) ||
                             %w[private group supergroup] # 'channel' es otra opción
            @callback = callback
        end

        def check(msg)
            return unless msg.is_a? Telegram::Bot::Types::Message

            @types.each do |type|
                atributes = msg.send type
                return true if atributes && !(atributes.is_a?(Array) && atributes.empty?)
            end
            false
        end

        def run(bot, msg)
            bot.public_send(@callback, msg)
        end
    end

    class InlineQuery
        def initialize(callback)
            @callback = callback
        end

        def check(query)
            query.is_a? Telegram::Bot::Types::InlineQuery
        end

        def run(bot, query)
            bot.public_send(@callback, query)
        end
    end
end
