require 'telegram/bot'

class DankieLogger
    attr_reader :client
    attr_accessor :logger

    def initialize(logger, channel_logging)
        @logger = logger
        @channel_logging = channel_logging
    end

    def initialize_client(client)
        @tg = client
    end

    def debug(text, to_channel: false, backtrace: nil, parse_html: true, resp_to: nil)
        log(
            level: Logger::DEBUG,
            text: text,
            to_channel: to_channel,
            backtrace: backtrace,
            parse_html: parse_html,
            resp_to: resp_to
        )
    end

    def warn(text, to_channel: false, backtrace: nil, parse_html: true, resp_to: nil)
        log(
            level: Logger::WARN,
            text: text,
            to_channel: to_channel,
            backtrace: backtrace,
            parse_html: parse_html,
            resp_to: resp_to
        )
    end

    def info(text, to_channel: false, backtrace: nil, parse_html: true, resp_to: nil)
        log(
            level: Logger::INFO,
            text: text,
            to_channel: to_channel,
            backtrace: backtrace,
            parse_html: parse_html,
            resp_to: resp_to
        )
    end

    def error(text, to_channel: false, backtrace: nil, parse_html: true, resp_to: nil)
        log(
            level: Logger::ERROR,
            text: text,
            to_channel: to_channel,
            backtrace: backtrace,
            parse_html: parse_html,
            resp_to: resp_to
        )
    end

    def fatal(text, to_channel: false, backtrace: nil, parse_html: true, resp_to: nil)
        log(
            level: Logger::FATAL,
            text: text,
            to_channel: to_channel,
            backtrace: backtrace,
            parse_html: parse_html,
            resp_to: resp_to
        )
    end

    def unknown(text, to_channel: false, backtrace: nil, parse_html: true, resp_to: nil)
        log(
            level: Logger::UNKNOWN,
            text: text,
            to_channel: to_channel,
            backtrace: backtrace,
            parse_html: parse_html,
            resp_to: resp_to
        )
    end

    def exception_text(exception)
        text_exception = exception.to_s
        text = if text_exception.nil? || text_exception.empty?
                   'NO NAME EXCEPTION'
               else
                   "(#{exception.class}) #{text_exception}"
               end

        return text, nil if exception.backtrace.nil?

        [text, exception.backtrace.join("\n").gsub(%r{/home/[^/]+}, '~')]
    end

    private

    def log(params)
        level = params[:level]
        text = params[:text]
        backtrace = params[:backtrace]

        text = 'LOG SIN NOMBRE' if text.nil? || text.empty?

        log_backtrace(backtrace, level, text)
        return unless params[:to_channel]

        # Creo el text del logging
        text = html_parser(text) if params[:parse_html]
        unless backtrace.nil?
            lines = "<pre>#{'-' * 30}</pre>\n"
            text << "\n#{lines}#{lines} Rastreo de la excepción:\n#{lines}"
            text << "<pre>#{html_parser(backtrace)}</pre>"
        end

        level = log_level(level)

        time = Time.now.strftime('%FT%T.%6N')
        lines = "<pre>#{'-' * (8 + time.length + level.length)}</pre>\n"

        to_send = "<pre>[#{time}] -- #{level} :</pre>\n#{lines}#{text}"
        @tg.send_message(chat_id: @channel_logging, text: to_send,
                                 parse_mode: :html, disable_web_page_preview: true,
                                 reply_to_message_id: params[:resp_to])
    rescue StandardError => e
        begin
            check_multiple_exception(backtrace, e)
        rescue StandardError
            puts "\nFATAL, lots of exceptions.\n"
        end
    end

    def log_backtrace(backtrace, level, text)
        if backtrace.nil?
            @logger.log(level, text)
        else
            @logger.log(level, "#{text}\n#{backtrace}")
        end
    end

    def log_level(level)
        case level
        when Logger::DEBUG
            'DEBUG'
        when Logger::INFO
            'INFO'
        when Logger::WARN
            'WARN'
        when Logger::ERROR
            'ERROR'
        when Logger::FATAL
            'FATAL'
        when Logger::UNKNOWN
            'UNKNOWN'
        else
            'WRONG LOG LEVEL'
        end
    end

    def check_multiple_exception(backtrace, exc)
        message = if backtrace.nil? || backtrace.empty?
                    then "\nDuring the handling of an exception:\n"
                  else
                      "#{backtrace}\n\n\nAnother was raised\n\n"
                  end

        lines = "#{'-' * 30}\n"
        text_exception = lines + message

        exception = exc.to_s
        text_exception << if exception.nil? || exception.empty?
                              'NO NAME ERROR'
                          else
                              exception
                          end

        text_exception << "\n#{lines}#{lines}#{exc.backtrace.join("\n")}\n"\
                          "#{lines}#{lines}\n"
        @logger.fatal(text_exception)
    end

    def html_parser(text)
        CGI.escapeHTML(text)
    end
end
