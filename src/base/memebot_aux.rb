class MemeBot

    def invalid_chat(msg, valid_chats)
        return if msg.chat.type == 'channel'

        translates = { 'private' => 'privado', 'group' => 'grupos',
                         'supergroup' => 'supergrupos', 'channel' => 'canales' }

        text = "Este comando es válido solo en #{translates[valid_chats.first]}"

        case valid_chats.length
        when 2
            text << " y #{translates[valid_chats[1]]}"
        when 3
            text << ", #{translates[valid_chats[1]]} y #{translates[valid_chats[2]]}"
        end

        @tg.send_message(
            chat_id: msg.chat.id,
            reply_to_message_id: msg.message_id,
            text: text
        )
    end

    private

    def parse_keywords(params)
        # Clean useless chars
        keywords = params.downcase.gsub(/[^0-9[[:lower:]]\s]/, '')
        keywords.split.join(' ')
    end

    def create_msg_keyboard_temp_meme(msg, keywords, video_id, action)
        name_user = user_link(msg.from, msg.chat.id) || 'eliminado'
        date = Time.at(msg.date, in: @tz.utc_offset)

        text = date.strftime("<code>[%d/%m/%Y %T]</code>\n")
        text << "#{name_user} (#{msg.from.id}) en "\
                "#{get_chat_title msg} (#{msg.chat.id}) "\
                "quiere #{action} el video meme: "\
                "<code>#{keywords}</code>\n\nvideo_id: <b>#{video_id}</b>\n"
        text
    end

    def authorized(action_request, callback: false, action: nil, lower_perm: false)
        allowed = lower_perm ? AUTORITIES.union(LOWER_PERMISSIONS) : AUTORITIES

        unless allowed.member? action_request.from.id
            error = "No tienes autorización para #{action || 'eso'}."

            if callback
                @tg.answer_callback_query(
                    callback_query_id: action_request.id,
                    show_alert: true,
                    text: error
                )
            else
                @tg.send_message(
                    chat_id: action_request.chat.id,
                    reply_to_message_id: action_request.message_id,
                    text: error
                )
            end

            return false
        end
        true
    end

    def self_callback_request(callback, temp_id)
        temp_key = "memes:temp:#{temp_id}"

        if callback.from.id == @redis.hget(temp_key, 'user_id').to_i
            @tg.answer_callback_query(
                callback_query_id: callback.id,
                show_alert: true,
                text: 'No puedes resolver las sugerencias que creaste, '\
                      'otra persona debe hacerlo'
            )
            return true
        end
        false
    end

    def can_press_keyboard?(callback_id, chat_id, message_id, state, error)
        if @redis.get("keyboard:#{chat_id}:#{message_id}") != state
            @tg.answer_callback_query(
                callback_query_id: callback_id,
                show_alert: true,
                text: error
            )
            return false
        end
        true
    end

    def user_link(user, chat)
        chat_id = chat.is_a?(Telegram::Bot::Types::Chat) ? chat.id : chat

        unless user.is_a?(Telegram::Bot::Types::User)
            begin
                chat_member = @tg.get_chat_member(chat_id: chat_id, user_id: user)
                user = Telegram::Bot::Types::ChatMember.new(chat_member['result']).user
            rescue Telegram::Bot::Exceptions::ResponseError => e
                @logger.error e.to_s, to_channel: true
                return 'Cuenta Eliminada'
            end
        end

        user_id = user.id
        username = user.username

        if user.first_name.empty?
            full_name = 'Cuenta Eliminada'
        else
            full_name = html_parser(
                user.first_name + (user.last_name ? " #{user.last_name}" : '')
            )
        end

        if username
            "<a href='https://telegram.me/#{username}'>#{full_name}</a>"
        else
            "<a href='tg://user?id=#{user_id}'>#{full_name}</a>"
        end
    end

    def board_array(params)
        iterable_set = params[:iterable_set]
        arr = params[:arr]
        subtitle = params[:subtitle]
        counter = params[:counter]
        max_lines = params[:max_lines]
        max_size = params[:max_size]

        return if iterable_set.nil? || iterable_set.empty?

        add_subtitle(params)

        iterable_set.each do |item|
            if arr.empty? || counter >= max_lines || arr.last.size >= max_size
                arr << params[:title].dup
                arr.last << subtitle.dup if subtitle
                counter = -1
            end
            arr.last << params[:code_block].call(item)
            counter += 1
        end
        counter
    end

    def add_subtitle(params)
        if params[:start_in_subtitle] && !params[:arr].empty? && params[:subtitle] &&
           params[:counter] < params[:max_lines] &&
           params[:arr].last.size < params[:max_size]
            params[:arr].last << "\n#{params[:subtitle].dup}"
        end
    end

    def get_chat_title(msg)
        if msg.chat.type == 'private'
            'su chat privado'
        else
            "el chat #{html_parser msg.chat.title}"
        end
    end
end
