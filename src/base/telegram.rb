require 'telegram/bot'
require 'httpclient'
require 'faraday/httpclient'

class TelegramAPI
    attr_reader :client, :token

    # token es String, logger es Logger
    def initialize(token, logger)
        Telegram::Bot.configure do |config|
            config.adapter = :httpclient
        end
        @client = Telegram::Bot::Client.new token, logger: logger
        @token = token
        @exceptions = ExceptionHandler.new logger
    end

    def catch(exception)
        @exceptions.catch exception
    end

    def send_message(args)
        return unless args[:chat_id] && args[:text] && !args[:text].empty?

        res = nil
        text = args[:text]

        start = 0
        finish = [text.length, 4096].min

        while start != finish
            args[:text] = text[start..(finish - 1)].strip

            unless args[:text].nil? || args[:text].empty?
                res = send_tg(:send_message, args, 'typing')
            end

            # indexes
            start = finish
            finish = [text.length, finish + 4096].min
        end
        res
    end

    def edit_message_text(args)
        # Chequeo que no se pase el tamaño
        if args[:text].length > 4096
            # Ver que onda con el tema de entidades html
            args[:text] = args[:text][0..4095]
        end
        args[:text].strip

        send_tg(:edit_message_text, args) unless args[:text].empty?
    end

    def forward_message(args)
        send_tg(:forward_message, args)
    end

    def send_photo(args)
        send_tg(:send_photo, args, 'upload_photo')
    end

    def send_audio(args)
        send_tg(:send_audio, args, 'upload_audio')
    end

    def send_document(args)
        send_tg(:send_document, args, 'upload_document')
    end

    def send_video(args)
        send_tg(:send_video, args, 'upload_video')
    end

    def send_animation(args)
        send_tg(:send_animation, args, 'upload_video')
    end

    def send_video_note(args)
        send_tg(:send_video_note, args, 'upload_video_note')
    end

    def send_voice(args)
        send_tg(:send_voice, args, 'upload_audio')
    end

    def send_location(args)
        send_tg(:send_location, args, 'find_location')
    end

    def send_sticker(args)
        send_tg(:send_sticker, args)
    end

    def send_media_group(args)
        send_tg(:send_media_group, args, 'upload_photo')
    end

    def answer_callback_query(args)
        send_tg :answer_callback_query, args
    end

    private

    def send_tg(function, args, action = nil)
        # Si hay una action que mandar, la mando
        if action
            @client.api.send_chat_action(chat_id: args[:chat_id],
                                         action: action)
        end
        # Mando el mensaje (de texto, sticker, lo que sea)
        @client.api.send(function, args)
    rescue Telegram::Bot::Exceptions::ResponseError => e
        if e.error_code.to_i == 400
            args[:reply_to_message_id] = nil

            retry_errors(e, args)
            retry
        else
            @exceptions.log(e, args)
            raise
        end
    end

    def retry_errors(exception, args)
        if exception.message.include?('reply message not found')
            @client.logger.error(
                'No puedo responder a un mensaje '\
                "borrado (ID: #{args[:reply_to_message_id]}) "\
                "en #{args[:chat_id]}. Error:\n#{exception.message}"
            )
        elsif exception.message.include?('group chat was upgraded to a supergroup chat')
            init = exception.message.split('{"migrate_to_chat_id"=>').last
            supergroup = init.split('}').first

            @client.logger.error("Error en #{args[:chat_id]}. El grupo se "\
                                 'actualizó y ahora es un supergrupo '\
                                 "(#{supergroup}).\n#{exception.message}",
                                 to_channel: true)
            args[:chat_id] = supergroup.to_i
        else
            raise
        end
    end

    # Tengo acceso a toda la api de telegram (bot.api) desde esta clase
    def method_missing(method_name, *args)
        super unless @client.api.respond_to?(method_name)
        @client.api.send(method_name, *args)
    rescue Telegram::Bot::Exceptions::ResponseError => e
        @exceptions.log(e, args)
        raise
    end

    def respond_to_missing?(method_name)
        @client.api.respond_to?(method_name) || super
    end
end
