# Magic data to start
require 'yaml'
# Loading bot
require_relative 'src/base/memebot'
# Load all modules
Dir["#{File.dirname(__FILE__)}/src/modules/*.rb"].each { |file| require_relative file }

# Poot Dir in root folder
Dir.chdir File.dirname($PROGRAM_NAME)

config = YAML.load_file(File.join(__dir__, 'data/config.yml'))
config.transform_keys!(&:to_sym)

memebot = MemeBot.new config
memebot.run
